<?php

namespace criamodels;

use Illuminate\Database\Eloquent\Model;

class Referencias extends Model
{
        protected $fillable = [
        'id_projeto', 'tabela_atual', 'campo_atual','tabela_estrangeira','campo_estrangeiro'
    ];


    protected $hidden = [
        'id'
    ];

}
