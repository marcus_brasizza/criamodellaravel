<?php

namespace criamodels\Http\Controllers;

use Illuminate\Http\Request;
use criamodels\Helpers\DatabaseConnection;
use criamodels\Referencias;


class TabelasController extends Controller
{
	private $name = 'tabelas';

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function ModelExistente(&$dados_session){
		$caminho_projeto = $dados_session['caminho_projeto'].'/app/';
		foreach($dados_session['tabelas'] as &$tabelas){
			$tabela = $tabelas['tabela'];
			if($tabela == 'users'){
				$tabelas['users'] = true;
			}else{
				$tabelas['users'] = false;
			}
			$nTable = explode('_',$tabela);
			$classname = '';
			foreach($nTable as $n){
				$classname.=ucfirst($n);
			}
			$arquivo = "{$caminho_projeto}{$classname}.php";
			if(file_exists($arquivo)){
				$existe = 'sim';
			}else{
				$existe = 'nao';
			}
			$tabelas['existe'] = $existe;
		}
	}
	public function index(Request $request){
			if($request->session()->get('namespace') == null){
				return redirect()->action('ConfiguracaoController@index');
			}
		$search_params = CommonsControllers::createParams($request,$this->name);
		$params=$search_params['params'];
		$dados_session  = $search_params['session'];
		$this->ModelExistente($dados_session);
		return view('tabelas')->with($dados_session);
	}

	public function salvarReferencias(Request $request){
		$dados = ($request->all());
		$dados['id_projeto'] = $dados['id'];
		unset($dados['id']); 

		$referencia = Referencias::where('id_projeto','=',$dados['id_projeto'])->where('tabela_atual','=', $dados['tabela_atual'])->where('campo_atual','=', $dados['campo_atual'])->first();
		if($referencia != null){
			$referencia->delete();
		}
		$ref  = Referencias::create($dados);
	}

	public function procurarTabela(Request $request , $tabela,$json=null){
		$search_params = CommonsControllers::createParams($request,$this->name);
		$params=$search_params['params'];
		$dados_session  = $search_params['session'];
		$dados_session['id'] = $request->session()->get('id');
		$dados_session['ajax'] = true;
		$campos_tabela = DatabaseConnection::setConnection($params)->select("show fields from {$tabela}");
		foreach($campos_tabela as $idx=>&$campos){


			$type = ($campos->Type);
			$find = explode('(',$type);
			$campos->Normatizado = $find[0];
			$referencia = Referencias::where('id_projeto','=',$dados_session['id'])->where('tabela_atual' , '=' , $tabela)->where('campo_atual','=',$campos->Field)->first();

				// [['id_projeto','=',$dados_session['id']],['tabela_atual','=', $tabela]],['campo_atual','=', $campos->Field]
			$campos->TabelaAtual = $tabela;
			if($referencia){
				$campos->TabelaEstrangeira = $referencia->tabela_estrangeira;
				$campos->CampoEstrangeiro = $referencia->campo_estrangeiro;
			}else{
				$campos->TabelaEstrangeira = null;
				$campos->CampoEstrangeiro = null;
			}
		}
		$dados_session['campos_tabela'] = $campos_tabela;
		$dados_session['tabela_atual'] = $tabela;
		
		if($json != null){
			return json_encode(
				array(
					'tabela'=>$tabela,
					'campos'=>$campos_tabela
				)
			);
		}
		return view('listagem_campos_tabela')->with($dados_session);
	}


	public function makeControllerFile($tabela,$dados_session){
		$classname = '';
		$namespace = $dados_session['projeto'];
		$nTable = explode('_',$tabela);
		foreach($nTable as $n){
			$classname.=ucfirst($n);
		}
		$classNameController = $classname.'Controller';
		$usemodel = 'use '.$namespace.'\\'.$classname.';'.PHP_EOL;
		$dados_controller_model = file_get_contents('modelos/modelo_controller.php');
		$dados_controller_model = str_replace('#namespace#', $namespace, $dados_controller_model);
		$dados_controller_model = str_replace('#classname# ', $classNameController, $dados_controller_model);
		$referencias = Referencias::where('tabela_atual', '=',$tabela)->where('id_projeto','=',$dados_session['id'])->distinct()->get();
		if($referencias){
			foreach ($referencias as $key => $referencia) {
				$classname = '';
				$nTable = explode('_',$referencia->tabela_estrangeira);
				foreach($nTable as $n){
					$classname.=ucfirst($n);
				}

				$usemodel .= 'use '.$namespace.'\\'.$classname.';'.PHP_EOL;

			}
		}
		$dados_controller_model = str_replace('#usemodel#', $usemodel, $dados_controller_model);
		if(file_exists($dados_session['caminho_projeto'].'/app/Http/Controllers/'.$classNameController.'.php')){
			$conteudo_antigo = file_get_contents($dados_session['caminho_projeto'].'/app/Http/Controllers/'.$classNameController.'.php');
			$this->getCustomSpot('custom','insideCustom',$conteudo_antigo,$dados_controller_model);
		}else{
			$dados_controller_model = str_replace('#insideCustom#', '', $dados_controller_model);
		}
		file_put_contents($dados_session['caminho_projeto'].'/app/Http/Controllers/'.$classNameController.'.php', $dados_controller_model);

	}

	public function makeModelFile($tabela,$dados_session,$timestamp = false){
		$classname = '';
		if($tabela == 'users'){
			return ;
		}
		$namespace = $dados_session['projeto'];
		$nTable = explode('_',$tabela);
		foreach($nTable as $n){
			$classname.=ucfirst($n);
		}
		$fields = '';
		$update  = false;
		$create = false;
		foreach($dados_session['campos_tabela'] as $campos){

			$fields .=  "'{$campos->Field}',";
			if($campos->Field == 'update_at'){
				$update = true;
			}

			if($campos->Field == 'created_at'){
				$create = true ;
			}

		}
		$fields = substr($fields,0,strlen($fields)-1);
		$tablenames = "protected \$table = '$tabela'	;".PHP_EOL;
		$tablePrimary = "protected \$primaryKey  = '{$dados_session['campos_tabela'][0]->Field}';".PHP_EOL;
		if($create == false || $create == false){
			$tableTimestamp = 'public $timestamps = false;'.PHP_EOL;
		}else{
		$tableTimestamp = "";
	}
		$fillable = "protected \$fillable = array({$fields});";
		$dados_modelo_model = file_get_contents('modelos/modelo_model.php');
		$dados_modelo_model = str_replace('#namespace#', $namespace, $dados_modelo_model);
		$dados_modelo_model = str_replace('#classname# ', $classname, $dados_modelo_model);
		$dados_modelo_model = str_replace('#tablename#', $tablenames, $dados_modelo_model);
		$dados_modelo_model = str_replace('#primaryKey#', $tablePrimary, $dados_modelo_model);
		$dados_modelo_model = str_replace('#fillable#', $fillable, $dados_modelo_model);
		if($timestamp == false){

			$dados_modelo_model = str_replace('#timestamp#', $tableTimestamp, $dados_modelo_model);

		}else{
			$dados_modelo_model = str_replace('#timestamp#', '', $dados_modelo_model);

		}
		if(file_exists($dados_session['caminho_projeto'].'/app/'.$classname.'.php')){
			$conteudo_antigo = file_get_contents($dados_session['caminho_projeto'].'/app/'.$classname.'.php');

			$this->getCustomSpot('custom','insideCustom',$conteudo_antigo,$dados_modelo_model);
			$this->getCustomSpot('customRef','insideCustomRef',$conteudo_antigo,$dados_modelo_model);
		}else{

			$dados_modelo_model = str_replace('#insideCustom#', '', $dados_modelo_model);
			$dados_modelo_model = str_replace('#insideCustomRef#', '', $dados_modelo_model);

		}

		$this->gerarReferencias($dados_session,$namespace,$tabela,$dados_modelo_model);
		file_put_contents($dados_session['caminho_projeto'].'/app/'.$classname.'.php', $dados_modelo_model);
		return true;
	}


	public function gerarReferencias($dados_session,$namespace,$tabela,&$dados_modelo_model){
			$code = '';
					$referencias = Referencias::where('tabela_atual', '=',$tabela)->where('id_projeto','=',$dados_session['id'])->distinct()->get()->toArray();
		$referencias_estrangeira = Referencias::where('tabela_estrangeira', '=',$tabela)->where('id_projeto','=',$dados_session['id'])->distinct()->get()->toArray();

		
		if($referencias){

			foreach ($referencias as $key => $referencia) {

				$classname = '';
				$nTable = explode('_',$referencia['tabela_estrangeira']);
				foreach($nTable as $n){
					$classname.=ucfirst($n);
				}

				if($classname == 'Users'){
					$classname = 'User';//PADRAO DO LARAVEL
				}
				$code.= 'public function __'.$referencia['tabela_estrangeira'].'(){'.PHP_EOL."\t\t".'return $this->belongsTo(\''.$namespace.'\\'.$classname.'\',\''.$referencia['campo_atual'].'\',\''.$referencia['campo_estrangeiro'].'\');'.PHP_EOL."\t".'}'.PHP_EOL."\t";

						 
			}
		
		}
			if($referencias_estrangeira){
				foreach ($referencias_estrangeira as $key => $referencia) {

				$classname = '';
				$nTable = explode('_',$referencia['tabela_atual']);
				foreach($nTable as $n){
					$classname.=ucfirst($n);
				}

				if($classname == 'Users'){
					$classname = 'User';//PADRAO DO LARAVEL
				}
				$code.= 'public function __'.$referencia['tabela_atual'].'(){'.PHP_EOL."\t\t".'return $this->hasMany(\''.$namespace.'\\'.$classname.'\',\''.$referencia['campo_atual'].'\',\''.$referencia['campo_estrangeiro'].'\');'.PHP_EOL."\t".'}'.PHP_EOL."\t";

						 
			}

			}
			if($code != ''){
			$dados_modelo_model = str_replace('#referencias#', $code, $dados_modelo_model);
		}

	}

	public function getCustomSpot($tag ,$tagInside, $conteudo_antigo,&$dados_customizados){

		$re = '/#'.$tag.'#(.|\n*)+#'.$tag.'#/m';
		preg_match_all($re, $conteudo_antigo, $matches, PREG_SET_ORDER, 0);
		$resultado_custom = $matches[0][0] ;
		$resultado_custom= trim(str_replace('#'.$tag.'#','',$resultado_custom));
		$dados_customizados = str_replace('#'.$tagInside.'#', $resultado_custom, $dados_customizados);


	}

	public function gerarArquivo(Request $request , $tabela,$redirect = true){

		$search_params = CommonsControllers::createParams($request,$this->name);
		$params=$search_params['params'];
		$dados_session  = $search_params['session'];
		$campos_tabela = DatabaseConnection::setConnection($params)->select("show fields from {$tabela}");
		$dados_session['campos_tabela'] = $campos_tabela;
		$this->makeModelFile($tabela,$dados_session);
		$this->makeControllerFile($tabela,$dados_session);
		
		if($redirect){
			return redirect()->action('TabelasController@index');
		}
	}



	public function gerarTodosArquivos(Request $request){
		$search_params = CommonsControllers::createParams($request,$this->name);
		$params=$search_params['params'];
		$campos_tabela = DatabaseConnection::setConnection($params);
		$dados_session  = $search_params['session'];
		if((!isset($dados_session['tabelas'])) || (($dados_session['tabelas'] == ''))){
			return redirect()->action('HomeController@index');

		}

		foreach($dados_session['tabelas'] as $tabela){

			$this->gerarArquivo($request,$tabela['tabela'],false);
		}
		return redirect()->action('TabelasController@index');

	}

    //
}
