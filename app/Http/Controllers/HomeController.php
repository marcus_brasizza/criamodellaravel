<?php

namespace criamodels\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \criamodels\Projetos;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $dados = array(
                'pagina' => 'home'
            );
                $request->session()->forget(
            [
                'selecao', 
                'host',
                'username',
                'password',
                'path',
                'projeto',
                'database',
                'caminho_projeto',
                'pagina',
                'tabelas',
                'id'
            ]

        );
                    $user_id = Auth::user()->id;

                $projetos = Projetos::where('id_usuario','=',$user_id)->get();

                $dados['projetos'] = $projetos;  


        return view('home')->with($dados);
    }
}
