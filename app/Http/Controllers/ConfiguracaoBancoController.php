<?php

namespace criamodels\Http\Controllers;

use Illuminate\Http\Request;
use criamodels\Helpers\DatabaseConnection;
use criamodels\Projetos;
use criamodels\Referencias;
use DB;

class ConfiguracaoBancoController extends Controller
{

	private $name = 'banco';

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function index(Request $request){
			
			if($request->session()->get('namespace') == null){
				return redirect()->action('ConfiguracaoController@index');
			}
		
		$search_params = CommonsControllers::createParams($request,$this->name);
		$params=$search_params['params'];
		$dados_session  = $search_params['session'];
		$mensagem = $request->input('message');
		if($mensagem!= ''){
			$dados_session['exception'] = base64_decode($mensagem);
		}
		try{
			$bancos_host = DatabaseConnection::setConnection($params)->select("show databases");
			$dados_session['bancos_host'] = $bancos_host;
		}catch(\Exception $e){
			$dados_session['exception'] = $e->getMessage();
			return redirect()->action('ConfiguracaoController@index',['message'=>base64_encode($e->getMessage())]);
		}	

			if($request->session()->get('database') == null){
				$dados_session['database'] = '';
			}


		return view('configuracaoBanco')->with($dados_session);
	}

	public function makeEnv($env_file,$session_params){
		if(file_exists($session_params['caminho_projeto'].'/.env')){
			return '';
		}
		$env = file_get_contents('.env_example');
		$env = str_replace('#NOMEPROJETO#',$session_params['projeto'],$env);
		$env = str_replace('#HOST#',$session_params['host'],$env);
		$env = str_replace('#USERNAME#',$session_params['username'],$env);
		$env = str_replace('#PASSWORD#',$session_params['password'],$env);
		$env = str_replace('#DATABASE#',$session_params['database'],$env);
		$env = str_replace('#base64#',base64_encode(uniqid()),$env);
		file_put_contents($session_params['caminho_projeto'].'/.env',$env);
	}
	public function normatizarTabelas($tabelas,$caminho,$dados_session){
		DB::table('referencias')->where('id_projeto', $dados_session['id'])->delete();
		$database = $dados_session['database'];
		foreach ($tabelas as $idx => $tabela) {
			$normalizada = (array)$tabela;
			$val = current(array_values($normalizada));
	
			$dados[] = array('tabela'=>$val);


		$tabela=DB::select('SELECT 
			  TABLE_NAME,COLUMN_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
			FROM
			  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
			WHERE
			  REFERENCED_TABLE_SCHEMA = "'.$database.'"
			  AND   REFERENCED_TABLE_NAME = "'.$val.'";
			  
			  ');
		if($tabela){
				$dadosReferencia = array();
			foreach ($tabela as $key => $referencia) {
				$dadosReferencia['id_projeto'] = $dados_session['id'];
				$dadosReferencia['tabela_atual'] = $referencia->TABLE_NAME;
				$dadosReferencia['campo_atual'] = $referencia->COLUMN_NAME; 
				$dadosReferencia['tabela_estrangeira'] = $referencia->REFERENCED_TABLE_NAME; 
				$dadosReferencia['campo_estrangeiro'] = $referencia->REFERENCED_COLUMN_NAME; 
				Referencias::create($dadosReferencia);
				unset($dadosReferencia);
				
			}
		}
		}
		return $dados;
	}
	public function salvarBanco(Request $request){
		$banco = ($request->input('banco'));
		$request->session()->put('database',$banco);
		$search_params = CommonsControllers::createParams($request);
		$dados_session = $search_params['session'];
		$params=$search_params['params'];
		$projeto = Projetos::find($request->session()->get('id'));
		$projeto['banco'] = $banco;
		$projeto->save();
		try{
			$tables_banco = DatabaseConnection::setConnection($params)->select("show tables");
			$this->makeEnv('.env_example',$dados_session);
			
			$tabelas = $this->normatizarTabelas($tables_banco,$dados_session['caminho_projeto'].'/app/',$dados_session);

			$request->session()->put('tabelas' , $tabelas);


			return redirect()->action('TabelasController@index');

			
		}catch(\Exception $e){
			return redirect()->action('ConfiguracaoBancoController@index',['message'=>base64_encode($e->getMessage())]);
		}

	}
    //


}
