<?php

namespace criamodels\Http\Controllers;

use Illuminate\Http\Request;

class CommonsControllers extends Controller
{


	public static function buildSessions(Request $request,$classname = ''){
		$selecao = ($request->session()->get('database'))?$request->session()->get('database'):'-1';
		$host = ($request->session()->get('host'))?$request->session()->get('host'):'';
		$user = ($request->session()->get('username'))?$request->session()->get('username'):'';
		$pass = ($request->session()->get('password'))?$request->session()->get('password'):'';
		$database = ($request->session()->get('database'))?$request->session()->get('database'):'';
		$path = ($request->session()->get('path'))?$request->session()->get('path'):'';
		$wwwSite = ($request->session()->get('wwwSite'))?$request->session()->get('wwwSite'):'http://'.$_SERVER['SERVER_NAME'].'/';
		$projeto = ($request->session()->get('projeto'))?$request->session()->get('projeto'):'';
		$caminho_projeto = ($request->session()->get('caminho_projeto'))?$request->session()->get('caminho_projeto'):'';
		$tabelas = ($request->session()->get('tabelas'))?$request->session()->get('tabelas'):'';
		$id = ($request->session()->get('id'))?$request->session()->get('id'):'';

		$dados_session = array(
			'selecao' => $selecao,
			'host' => $host,
			'username' => $user,
			'password' => $pass,
			'path' => $path,
			'projeto' => $projeto,
			'database' => $database,
			'caminho_projeto'=>$caminho_projeto,
			'pagina' => $classname,
			'tabelas' =>$tabelas,
			'id'=>$id
					);

	


	return $dados_session;
	}

		public static function createParams($request,$classname=''){
		$dados_session = self::buildSessions($request,$classname);
		$params = new  \stdClass();
		$params->driver='mysql';
		$params->host=$dados_session['host'];
		$params->username=$dados_session['username'];
		$params->password=$dados_session['password'];
		$params->database=(isset($dados_session['database']) && $dados_session['database'] != '')?$dados_session['database']:'';
		return array('params'=>$params,'session'=>$dados_session);;
	}
    //
}
