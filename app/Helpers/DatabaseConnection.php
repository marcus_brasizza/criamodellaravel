<?php

namespace criamodels\Helpers;
use Config;
use DB;

class DatabaseConnection
{
    public static function setConnection($params)
    {
        config(['database.connections.onthefly' => [
            'driver' => $params->driver,
            'host' => $params->host,
            'username' => $params->username,
            'password' => $params->password,
            'database' => $params->database
        ]]);

        return DB::connection('onthefly');
    }
}

?>