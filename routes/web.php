<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

//Rota de configuracao
Route::get('/carregar/{idprojeto}','ConfiguracaoController@carregarProjeto'); //Entra na listagem
Route::get('/configuracoes','ConfiguracaoController@index'); //Entra na listagem
Route::post("/configuracoes","ConfiguracaoController@salvarConfiguracao");

//Rota de banco de dados
Route::get('/banco' , "ConfiguracaoBancoController@index");
Route::post('/banco' , "ConfiguracaoBancoController@salvarBanco");

//Rota para tabelas
Route::get('/tabelas' , "TabelasController@index");
Route::get('/tabelas/gerar/todas' , "TabelasController@gerarTodosArquivos");
Route::get('/tabelas/gerar/{tabela}' , "TabelasController@gerarArquivo");
Route::get('/tabelas/search/{tabela}/{json?}' , "TabelasController@procurarTabela");
Route::post('/tabelas/salvarReferencias',"TabelasController@salvarReferencias");
