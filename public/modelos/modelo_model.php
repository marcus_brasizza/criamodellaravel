<?php
#infoData#
namespace #namespace#;

use Illuminate\Database\Eloquent\Model;

class #classname#  extends Model
{
	#tablename#
	#primaryKey#
	#fillable#
    #timestamp#
	//Put all your custom methods below and inside the custom tags
	//This custom tags will not be deleted after the generation
    #custom#
    #insideCustom#
    #custom#
    #referencias#
     #customRef#
    #insideCustomRef#
    #customRef#

}
