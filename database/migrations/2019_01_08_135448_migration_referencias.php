<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationReferencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('referencias', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('id_projeto');
            $table->string('tabela_atual');
            $table->string('campo_atual');
            $table->string('tabela_estrangeira');
            $table->string('campo_estrangeiro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
