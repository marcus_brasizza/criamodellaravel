<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">



    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

   <script>
    function loadme(tabela){
    //alert("loadig");
    $.ajax({url: "/tabelas/search/"+tabela, success: function(result){
        //alert("success"+result);
        $("#contentBody").html(result);
        $("#modalModel").modal('show'); 

    }});
}

function gerarArquivo(arquivo){
    con = confirm("Deseja gerar o arquivo de modelo de "+ arquivo+ "?");
    if(con){
      document.location.href="/tabelas/gerar/"+arquivo;
  }
}

function procurarCamposReferencia(tabela,posicao){
     $("#campo_tabela_"+posicao).html('');
       $("#campo_tabela_"+posicao).append($('<option>').text('--Escolha--').attr('value', ""));
  $.ajax({url: "/tabelas/search/"+tabela+'/json/',  dataType: 'json', success: function(json){
    

   


     $.each(json.campos, function(i, value) {  
        if(value.Normatizado == 'int' || value.Normatizado == 'bigint'){//SOMENTE INTEIROS DEVEM SER COLOCADOS COMO REFERENCIA
            $("#campo_tabela_"+posicao).append($('<option>').text(value.Field).attr('value', value.Field));
        }
    });
 }})
}

function salvarReferencia(tabela_atual,campo_atual,id_projeto,tabela_estrangeira,campo_estrangeiro){
    $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                type: "POST",
                url: 'tabelas/salvarReferencias',
                data: {
                    id:id_projeto,
                    tabela_atual:tabela_atual,
                    campo_atual:campo_atual,
                    tabela_estrangeira:tabela_estrangeira,
                    campo_estrangeiro:campo_estrangeiro
                },
                success: function(res){
                   console.log(res);
                },
                error: function(res){
                    console.log(res);
                }
            });

    
}

</script>
<div id="app">
    @if (!(isset($ajax)))
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('register'))
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    </li>
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link  " href="/" >Inicial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/configuracoes" >Configuracoes do projeto</a>
        </li>

        <li class="nav-item">
            <a class="nav-link " href="/banco">Banco de dados</a>
        </li>

        <li class="nav-item">
            <a class="nav-link   " href="/tabelas">Tabelas</a>
        </li>
    </ul>
</div>
</nav>
@endif
@yield('content')
</div>

</div>
</body>
</html>
