@extends('layouts.app')
@section('content')
<div class="container">

    @if (isset($exception))
    <div class="alert alert-danger">{{$exception}}</div>
    @endif
    <form class="form-horizontal" action="/banco" method="POST">
    @csrf


    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col" colspan="2" class="text-center">Configuraçoes</th>

      </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Url/Ip de conexao</th>
      <td>{{$host}}</td>
  </tr>
  <tr>
      <th scope="row">Usuario</th>

      <td>{{$username}}</td>
  </tr>
  <tr>
      <th scope="row">Caminho do projeto</th>
      <td>{{$path}}</td>
  </tr>
  <tr>
      <th scope="row">Bancos encontrados</th>
      <td>
        <select class="form-control"  name="banco">
            @foreach($bancos_host as $databases)
            <option {{( $databases->Database == $database ) ? 'selected':''}}>{{$databases->Database}}</option>
            @endforeach
        </select>
    </td>
</tr>
</tbody>
</table>
 <div class="form-group">
    <div class="col-sm-offset-2 col-12">
      <button type="submit" class="btn btn-default">Salvar</button>
    </div>
  </div>
</form> 

</div>
@endsection
