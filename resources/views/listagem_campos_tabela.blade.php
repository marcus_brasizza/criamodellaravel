@extends('layouts.app')
@section('content')
<div class="container">

  @if (isset($exception))
  <div class="alert alert-danger">{{$exception}}</div>
  @endif
  <form class="form-horizontal" id="listagem_campos" action="" method="POST">
    @csrf
    <input type="hidden" name="tabela" value="{{$tabela_atual}}">
    <input type="hidden" name="id" value="{{$id}}">
  
  <div>
  </div>

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col" colspan="2" class="text-center">Visualização dos campos da tabela</th>
      </tr>
      <tr>
        <th>Campo</th>
        <!-- <th>Tipo</th> -->
        <th>Tabela de ligaçao</th>
        <th>Campo</th>
      </tr>
      @foreach($campos_tabela as $idx=>$campos)
      <tr>
        <td> {{$campos->Field}}</td>
        <input type="hidden" name="campos[{{$idx}}]" value="{{$campos->Field}}">
        <!-- <td>{{$campos->Type}}</td> -->
        @if(($campos->Normatizado == 'int' || $campos->Normatizado == 'bigint') && ( $campos->Extra != "auto_increment" ))
        <td>
          <select id='tabela_estrangeira_{{$idx}}' name='tabela_estrangeira_{{$idx}}' class="form-control" onchange="procurarCamposReferencia(this.value,'{{$idx}}')">
            <option value="">--Escolha--</option>
            @foreach($tabelas as $tabela)
            @if($tabela['tabela'] != $tabela_atual)
            <option value="{{$tabela['tabela']}}" {{($campos->TabelaEstrangeira == $tabela['tabela']? 'selected' : '') }}>{{$tabela['tabela']}}</option>
            @endif
            @endforeach
          </select>
        </td>
        <td>
          <select id="campo_tabela_{{$idx}}" name="campo_tabela_{{$idx}}" class="form-control" onchange="salvarReferencia(
          '{{$tabela_atual}}','{{$campos->Field}}','{{$id}}',$('#tabela_estrangeira_{{$idx}}').val(),this.value


          )">





            

            @if($campos->CampoEstrangeiro != null)
            <option value="{{$campos->CampoEstrangeiro }}">{{$campos->CampoEstrangeiro }}</option>
            @else
            <option value="">--Escolha--</option>
            @endif
          </select>
        </td>
        @else
        <td colspan="2"></td>
        @endif
      </tr>
      @endforeach
    </thead>
  </table>
</form>
</div>
@endsection
