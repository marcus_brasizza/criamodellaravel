@extends('layouts.app')
@section('content')
<div class="container">

    @if (isset($exception))
    <div class="alert alert-danger">{{$exception}}</div>
    @endif
  <form class="form-horizontal" action="/configuracoes" method="POST">
        @csrf
        <input type="hidden"  id="id" name='id'  value="{{$id}}">
  <div class="form-group">
    <label class="control-label " for="host">Url do banco de dados:</label>
    <div class="col-12">
      <input type="text" class="form-control" id="host" name='host' placeholder="Url do banco" required="true" value="{{$host}}">
    </div>
  </div>

    <div class="form-group">
    <label class="control-label " for="user">Usuario do banco:</label>
    <div class="col-12">
      <input type="text" class="form-control" id="user" name='username' placeholder="Usuario do banco de dados" required="true" value="{{$username}}">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label" for="pwd">Senha:</label>
    <div class="col-12">
      <input type="password" class="form-control" id="pwd" name='password' placeholder="Senha do banco de dados" required="true" value="{{$password}}">
    </div>
  </div>


  <div class="form-group">
    <label class="control-label " for="path">Diretorio raiz com permissão para salvar seus arquivos: (/var/www/meusArquivos/)</label>
    <div class="col-12">
      <input type="text" class="form-control" id="path" name='path' placeholder="Pasta raiz do seu projeto" required="true" value="{{$path}}">
    </div>
  </div>

    <div class="form-group">
    <label class="control-label " for="path">Namespace do seu projeto</label>
    <div class="col-12">
      <input type="text" class="form-control" id="projeto" name='projeto' placeholder="Pasta raiz do seu projeto" required="true" value="{{$projeto}}">
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-12">
      <button type="submit" class="btn btn-default">Salvar</button>
    </div>
  </div>
</form> 
</div>
@endsection
