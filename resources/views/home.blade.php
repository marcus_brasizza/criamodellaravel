@extends('layouts.app')

@section('content')

<div class="py-5">
    <div class="container">
      <div class="row hidden-md-up">
      	@foreach($projetos as $projeto)

        <div class="col-md-4" style="padding-bottom: 10px;">
          <div class="card">
            <div class="card-block">
              <h4 class="card-title">{{$projeto['projeto']}}</h4>
              <h6 class="card-subtitle text-muted">caminho: <strong>{{$projeto['path']}}</strong></h6>
              <p class="card-text p-y-1">banco utilizado: <strong>{{$projeto['banco']}}</strong></p>
              <a href="/carregar/{{$projeto['id']}}" class="card-link">Utilizar</a>
            </div>
          </div>
        </div>
         @endforeach
      
    </div>
  </div>

@endsection
