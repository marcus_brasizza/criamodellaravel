@extends('layouts.app')
@section('content')
<div class="container">
 

@if (isset($exception))
<div class="alert alert-danger">{{$exception}}</div>
@endif
<form class="form-horizontal" action="/banco" method="POST">
  @csrf


  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col" colspan="2" class="text-center">Visualização das tabelas/ORM</th>

      </tr>
      <tr>
        <th>Tabela</th>
        <th>Gerado</th>
        <th>#</th>
      </tr>
    </thead>
    <tbody>
     @foreach($tabelas as $tabela)
     <tr>
      <td>
        {{$tabela['tabela']}}
      </td>
      <td>
        @if($tabela['users'] == false)
        @if($tabela['existe'] == 'sim')
          <a href="javascript:void(0)" onclick="gerarArquivo('{{$tabela['tabela']}}')">
        <i class="fa fa-thumbs-up"></i>
      </a>
        @else
        <a href="javascript:void(0)" onclick="gerarArquivo('{{$tabela['tabela']}}')">
        <i class="fa fa-thumbs-down">
        </i>
      </a>
        @endif
        @else
        <span>Sem geração</span>
        @endif
      </td>
      <td>
        <a href="javascript:void(0);">
          <i class="fa fa-search" onclick="loadme('{{$tabela['tabela']}}')"></i>
        </a>
      </td>

    </tr>
    @endforeach

  </tbody>
</table>
<div class="form-group">
  <div class="col-sm-offset-2 col-12">
    <button type="button" class="btn btn-default" onclick="document.location='/tabelas/gerar/todas'">Gerar tudo</button>
  </div>
</div>

</form> 

</div>


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="modalModel" tabindex="-1" role="dialog" aria-labelledby="modalModelLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalModelLabel">Lista de campos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="contentBody">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
@endsection
